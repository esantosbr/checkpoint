# CheckPoint

# bkpgw project

In Expert mode, download the script file and make it executable:

curl_cli -k https://gitlab.com/esantosbr/checkpoint/raw/master/bkpgw > /usr/bin/bkpgw && chmod +x /usr/bin/bkpgw

Run with the following command:

bkpgw

A file named "bkp_hostname_date_time.log" will be generated in the same directory the command was ran.